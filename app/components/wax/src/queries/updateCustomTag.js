import React from 'react'
import { Mutation } from '@apollo/client/react/components'
import { gql } from '@apollo/client'

const UPDATE_CUSTOM_TAG = gql`
  mutation UpdateCustomTag($input: [CustomTagUpdateInput]!) {
    updateCustomTag(input: $input) {
      id
      label
      tagType
    }
  }
`

const updateCustomTagMutation = props => {
  const { render } = props

  return (
    <Mutation mutation={UPDATE_CUSTOM_TAG}>
      {updateCustomTag => render({ updateCustomTag })}
    </Mutation>
  )
}

export default updateCustomTagMutation
